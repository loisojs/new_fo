// FOS Client

/*EXPORT*/ funcdef bool DropMenuNeedShowButton( Critter cr, Item item );
/*EXPORT*/ funcdef void DropMenuButtonAction( Critter cr, Item item );

class DropButton
{
    int                    SortIndex;
    Sprite::Sprite         PicNormal;
    Sprite::Sprite         PicActive;
    DropMenuNeedShowButton NeedShow;
    DropMenuButtonAction   Action;
}

class FixedDropMenu : Gui::IDropMenu
{
    int        MouseButton;
    CursorType Cursor;
    uint       ShowTime;
    int        ButtonHeight;
    string     ArtDir;
    string     OpenSound;
    DropButton[] Buttons;
    DropButton[] ShowButtons;
    bool   Pressed;
    int    PressedOffsetY;
    uint   MouseStayTick;
    Entity SelectedEntity;

    FixedDropMenu( int mouseButton, CursorType cursor, uint showTime, uint buttonHeight, string artDir, string openSound )
    {
        MouseButton = mouseButton;
        Cursor = cursor;
        ShowTime = showTime;
        ButtonHeight = buttonHeight;
        ArtDir = artDir;
        OpenSound = openSound;
        Buttons = array< DropButton >();
        ShowButtons = array< DropButton >();
        Pressed = false;
        PressedOffsetY = 0;
        MouseStayTick = 0;
        SelectedEntity = null;
    }

    void AddButton( int sortIndex, DropMenuNeedShowButton needShow, DropMenuButtonAction action, string picNormal, string picActive )
    {
        DropButton button = DropButton();
        button.SortIndex = sortIndex;
        button.PicNormal = Sprite::Sprite( ArtDir + picNormal );
        button.PicActive = Sprite::Sprite( ArtDir + picActive );
        @button.NeedShow = needShow;
        @button.Action = action;
        Buttons.insertLast( button );
        // Todo: sort
    }

    private Entity get_SafeSelectedEntity()
    {
        // Simulate entity weak reference
        if( SelectedEntity !is null && SelectedEntity.IsDestroyed )
            SelectedEntity = null;
        return SelectedEntity;
    }

    void Draw()
    {
        if( ShowButtons.length() > 0 )
        {
            int x = __MouseX + 20;             // Todo: rework to actual cursor size
            int y = __MouseY + 20;
            int activeIndex = PressedOffsetY / ButtonHeight;
            activeIndex = CLAMP( activeIndex, 0, int( ShowButtons.length() ) - 1 );
            for( uint i = 0; i < ShowButtons.length(); i++ )
            {
                DropButton     button = ShowButtons[ i ];
                Sprite::Sprite pic = ( int(i) == activeIndex ? button.PicActive : button.PicNormal );
                pic.Draw( x, y );
                y += ButtonHeight;
            }
        }
    }

    void MouseDown( int button )
    {
        if( MouseButton == button && Cursor == __Cursor && SafeSelectedEntity !is null )
        {
            Pressed = true;
            PressedOffsetY = 0;
            MouseStayTick = GetTick();
        }
    }

    void MouseUp( int button )
    {
        if( MouseButton == button && SafeSelectedEntity !is null )
        {
            Pressed = false;

            if( ShowButtons.length() == 0 )
                CollectShowButtons( true );

            int        activeIndex = PressedOffsetY / ButtonHeight;
            activeIndex = CLAMP( activeIndex, 0, int( ShowButtons.length() ) - 1 );
            DropButton dropButton = ShowButtons[ activeIndex ];
            ShowButtons.clear();

            Entity entity = SafeSelectedEntity;
            SelectedEntity = null;

            if( !entity.IsDestroyed )
            {
                Critter cr = cast< Critter >( entity );
                Item    item = cast< Item >( entity );
                if( dropButton.NeedShow( cr, item ) )
                    dropButton.Action( cr, item );
            }
        }
    }

    bool PreMouseMove( int offsetX, int offsetY )
    {
        if( Pressed )
        {
            if( offsetX != 0 || offsetY != 0 )
            {
                PressedOffsetY += offsetY;
                CustomCall( "SetMousePos " + ( __MouseX - offsetX ) + " " + ( __MouseY - offsetY ) + " false" );
            }
            return true;
        }
        return false;
    }

    void PostMouseMove( int offsetX, int offsetY )
    {
        if( Cursor == __Cursor )
        {
            Entity prevEntity = SafeSelectedEntity;
            SelectedEntity = null;

            Gui::Object hoveredObj = Gui::GetHoveredObject();
            Gui::Screen screen = Gui::GetActiveScreen();
            if( hoveredObj !is null )
            {
                if( hoveredObj.Grid !is null && cast< Gui::ItemView >( hoveredObj.Grid ) !is null )
                    SelectedEntity = cast< Gui::ItemView >( hoveredObj.Grid ).GetItem( hoveredObj.CellIndex );
            }
            else if( screen is null || !screen.IsModal )
            {
                Entity entity = GetMonitorEntity( __MouseX, __MouseY );
                if( entity !is null && ( cast< Critter >( entity ) !is null || cast< Item >( entity ) !is null ) )
                    SelectedEntity = entity;
            }

            if( prevEntity !is SafeSelectedEntity || offsetX != 0 || offsetY != 0 )
            {
                MouseStayTick = GetTick();
                ShowButtons.clear();
            }
        }
    }

    void InputLost()
    {
        Pressed = false;
        SelectedEntity = null;
        ShowButtons.clear();
    }

    void Loop()
    {
        if( SafeSelectedEntity !is null )
        {
            if( Cursor != __Cursor )
            {
                Pressed = false;
                SelectedEntity = null;
                ShowButtons.clear();
            }
            else if( MouseStayTick != 0 && GetTick() - ShowTime >= MouseStayTick )
            {
                MouseStayTick = 0;

                CollectShowButtons( !Pressed );

                if( ShowButtons.length() == 0 )
                {
                    Pressed = false;
                    SelectedEntity = null;
                }
            }
        }
    }

    private void CollectShowButtons( bool onlyFirst )
    {
        ShowButtons.clear();
        Critter cr = cast< Critter >( SafeSelectedEntity );
        Item    item = cast< Item >( SafeSelectedEntity );
        for( uint i = 0; i < Buttons.length(); i++ )
        {
            if( Buttons[ i ].NeedShow( cr, item ) )
            {
                ShowButtons.insertLast( Buttons[ i ] );
                if( onlyFirst )
                    break;
            }
        }
    }
}
